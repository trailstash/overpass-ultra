const queryUrl = () => {
  if (typeof window !== "undefined") {
    const params = new URLSearchParams(window.location.hash.slice(1));
    params.delete("map");
    return new URL("#" + params.toString(), window.location).toString();
  }
  return "#";
};
function inIframe () {
  if (typeof window !== "undefined") {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }
  return true;
}

const mapStyle = "https://styles.trailsta.sh/openmaptiles-osm.json";
const query = `/*
This is an example Overpass query.
Try it out by pressing the Run button above!
*/
[bbox:{{bbox}}];
(
way[highway=path];
way[highway=footway];
way[highway=cycleway];
way[highway=steps];
);
out geom;`;

const styles = [
  [
    "OSM OpenMapTiles",
    "https://styles.trailsta.sh/openmaptiles-osm.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "OSM Liberty",
    "https://styles.trailsta.sh/osm-liberty.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "OSM Bright",
    "https://styles.trailsta.sh/osm-bright.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "Positron",
    "https://styles.trailsta.sh/positron.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "Dark Matter",
    "https://styles.trailsta.sh/dark-matter.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "MapTiler Basic",
    "https://styles.trailsta.sh/maptiler-basic.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "Fiord Color",
    "https://styles.trailsta.sh/fiord-color.json",
    '<a href="https://openfreemap.org">OpenFreeMap</a>',
  ],
  [
    "Protomaps Light",
    "https://styles.trailsta.sh/protomaps-light.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "Protomaps Dark",
    "https://styles.trailsta.sh/protomaps-dark.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "Protomaps Data Viz (white)",
    "https://styles.trailsta.sh/protomaps-white.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "Protomaps Data Viz (grayscale)",
    "https://styles.trailsta.sh/protomaps-grayscale.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "Protomaps Data Viz (black)",
    "https://styles.trailsta.sh/protomaps-black.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "Basic",
    "https://styles.trailsta.sh/protomaps-contrast.json",
    '<a href="https://protomaps.com">Protomaps</a>',
  ],
  [
    "OpenTrailStash",
    "https://open.trailsta.sh/style.json",
    '<a href="https://trailsta.sh">TrailStash</a>',
  ],
  [
    "Tracestrack Carto",
    "https://tile.tracestrack.com/v/maps/carto/style.json?key=ec31d0d841df481a42ddf6ef56f955fb",
    '<a href="https://tracestrack.com">Tracestrack Maps</a>',
  ],
  [
    "Alidade Smooth",
    "https://tiles.stadiamaps.com/styles/alidade_smooth.json",
    '<a href="https://stadiamaps.com">Stadia Maps</a>',
  ],
  [
    "Alidade Smooth Dark",
    "https://tiles.stadiamaps.com/styles/alidade_smooth_dark.json",
    '<a href="https://stadiamaps.com">Stadia Maps</a>',
  ],
  [
    "Alidade Satellite",
    "https://tiles.stadiamaps.com/styles/alidade_satellite.json",
    '<a href="https://stadiamaps.com">Stadia Maps</a>',
  ],
  [
    "Stadia Outdoors",
    "https://tiles.stadiamaps.com/styles/outdoors.json",
    '<a href="https://stadiamaps.com">Stadia Maps</a>',
  ],
  [
    "Stamen Toner",
    "https://tiles.stadiamaps.com/styles/stamen_toner.json",
    '<a href="https://stamen.com/">Stamen Design</a>',
  ],
  [
    "Stamen Terrain",
    "https://tiles.stadiamaps.com/styles/stamen_terrain.json",
    '<a href="https://stamen.com/">Stamen Design</a>',
  ],
];


export const defaultMode = "ide";
export const modes = {
  ide: {
    query,
    styles,
    settings: {
      mapStyle,
      url: "https://overpass-ultra.us",
      mastodon: "https://mapstodon.space/@trailstash",
      zoom: 16,
      center: [-77.4515, 37.5287],
      options: {
        attributionControl: {
          customAttribution: "",
        },
        maxBounds: [
          [-179.999999999, -85.051129],
          [179.999999999, 85.051129],
        ],
      },
    },
  },
  map: {
    settings: {
      mapStyle,
      loadSettingsFromQueryParams: true,
      options: {
        attributionControl: {
          compact: true,
          customAttribution: `<a ${inIframe() ? 'target="_blank"' : ""} href=".">Ultra</a> (<a ${inIframe() ? 'target="_blank"' : ""} href="${queryUrl()}">View Query</a>)`,
        },
      },
    },
  },
};
