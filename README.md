# overpass-ultra.us config

This repo contains the [Ultra](https://gitlab.com/trailstash/ultra) [config](./config.mjs) & GitLab
Pages [config](.gitlab-ci.yml) for [overpass-ultra.us](https://overpass-ultra.us)
