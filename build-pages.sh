set -e
npm i
npm run build
npm run --prefix node_modules/@trailstash/ultra build:examples-docs
npm run --prefix node_modules/@trailstash/ultra build:maplibre-examples-docs
npm run --prefix node_modules/@trailstash/ultra build:docs
cp -r node_modules/@trailstash/ultra/dist/docs dist/.
mv dist public
